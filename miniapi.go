package main

import "fmt"
import "time"
import "net/http"
import "os"
import "log"
import "strings"



func Timer(w http.ResponseWriter, req *http.Request){
	if(req.Method != "GET"){
		fmt.Fprintf(w, "GET seulement")
		return
	}else{
		fmt.Fprintf(w, time.Now().Format("15h04"))
	}	
}

func Messenger(w http.ResponseWriter, req *http.Request){
	var author string
	var message string
	f, fileError := os.OpenFile("messages.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	req.ParseForm()

	if req.Method != "POST" {
		fmt.Fprintf(w, "POST seulement")
		return 
	}else{
		author = req.Form.Get("author")
		message = req.Form.Get("message")

		
			if author == "" || message == "" {
				log.Fatal("Parametres vides")
				fmt.Fprintf(w, "Parametres vides")
				return
			}
			if fileError != nil {
				log.Fatal(fileError)
				return
			}
	
			if _, fileError := f.Write([]byte(author + ":" + message + "\n")); fileError != nil {
				log.Fatal(fileError)
				return
			}
	
			if fileError := f.Close(); fileError != nil {
				log.Fatal(fileError)
				return
			}
			fmt.Fprintf(w, author+":"+ message)		

	}
}

func Reader(w http.ResponseWriter, req *http.Request){
	var lines [] string
	if req.Method != "GET" {
		fmt.Fprintf(w, "GET seulement")
		return
	}

	content, fileError := os.ReadFile("messages.txt")
	if fileError != nil {
		log.Fatal(fileError)
		return 
	}

	lines = strings.Split(string(content), "\n")

	for i := 0; i < len(lines); i++ {
		fmt.Fprintf(w, lines[i]+"\n")
	}

}

func main(){
	http.HandleFunc("/", Timer)
	http.HandleFunc("/messenger", Messenger)
	http.HandleFunc("/reader", Reader)
	http.ListenAndServe(":4567", nil)
}